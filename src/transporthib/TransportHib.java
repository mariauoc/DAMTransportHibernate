/*
 * Ejemplo con Hibernate
 */
package transporthib;

import org.hibernate.HibernateException;

/**
 *
 * @author stucom
 */
public class TransportHib {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            TransportORM gestor = new TransportORM();
            System.out.println("Iniciada sesión con Hibernate...");
            System.out.println("Test: Insertando ciudad...");
            City c = new City("08001", "Barcelona");
            gestor.insertCity(c);
            System.out.println("Ciudad dada de alta.");
            gestor.close();
            System.out.println("Cerrada sesión con Hibernate.");
        } catch (HibernateException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
