/*
 * Clase que se encarga de la persistencia con Hibernate
 */
package transporthib;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author stucom
 */
public class TransportORM {
    // Variable para mantener la sesión con la BBDD 
    // Similar a Connection en JDBC
    private Session sesion;
    
    // Variable para gestionar las transacciones con la bbdd
    private Transaction tx;

    public TransportORM() {
        // Establecemos la conexión
        sesion = HibernateUtil.getSessionFactory().openSession();
    }
    
    public void insertCity(City c) throws HibernateException {
        tx = sesion.beginTransaction();
        sesion.save(c);
        tx.commit();
    }
    
    public void updateCity(City c) {
        tx = sesion.beginTransaction();
        sesion.update(c);
        tx.commit();
    }
    
    public void removeCity(City c) {
        tx = sesion.beginTransaction();
        sesion.delete(c);
        tx.commit();
    }
    
    public List<City> selectAllCity() {
        Query q = sesion.createQuery("select c from City c");
        return q.list();
    }
    
//    select c.name, p.idpackage from City c, Package p
    
    public void close() {
        sesion.close();
    }
    
    
}
